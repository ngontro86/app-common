# app-common

A common set of tools to streamline injection, configuration, uncaught exception handling, as well as bootstrap applications. 
It is supposed to be Spring agnostic